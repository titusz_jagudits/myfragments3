package at.technikum.mse.se13m035.myfragments3;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;

import at.technikum.mse.se13m035.myfragments3.githubclient.Repo;

public class RepoDetailFragment extends Fragment
{
    public static final String GITHUB_REPO = "GITHUB_REPO";

    private static final String TAG = RepoDetailFragment.class.getName();

    private Repo repo;

    public RepoDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(GITHUB_REPO)) {
            //String githubUserName = bundle.getString(REPO_LIST);
            repo = (Repo) bundle.getSerializable(GITHUB_REPO);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView, container: " + container);
        Log.d(TAG, "onCreateView, container.parent: " + container.getParent());

        // load the layout
        View view = getView();
        Log.d(TAG, "onCreateView, getView(): " + view);
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_repo_detail, container, false);
        }
        Log.d(TAG, "onCreateView, view: " + view);

        if (repo != null) {
            // display the values
            TextView tvId = (TextView) view.findViewById(R.id.tvRepoId);
            tvId.setText(Integer.toString(repo.getId()));

            TextView tvFullName = (TextView) view.findViewById(R.id.tvRepoFullName);
            tvFullName.setText(repo.getFull_name());

            TextView tvRepoDescription = (TextView) view.findViewById(R.id.tvRepoDescription);
            tvRepoDescription.setText(repo.getDescription());

            TextView tvUpdatedAt = (TextView) view.findViewById(R.id.tvUpdatedAt);
            String currentDateTimeString = DateFormat.getDateTimeInstance().format(repo.getUpdated_at());
            tvUpdatedAt.setText(currentDateTimeString);
        }

        return view;
    }
}
