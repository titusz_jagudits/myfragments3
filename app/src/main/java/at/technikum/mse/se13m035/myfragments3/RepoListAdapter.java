package at.technikum.mse.se13m035.myfragments3;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import at.technikum.mse.se13m035.myfragments3.githubclient.Repo;

public class RepoListAdapter extends ArrayAdapter<Repo>
{
    private static final String TAG = ContributorListAdapter.class.getName();

    private Context context;
    private List<Repo> objects;

    public RepoListAdapter(Context context, int resource, List<Repo> objects) {
        super(context, resource, objects);
        this.context = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Repo repo = objects.get(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//        View view = inflater.inflate(R.layout.repo_list_item, null);
        View view = inflater.inflate(R.layout.repo_list_item, parent, false);

        TextView tvRepoId = (TextView) view.findViewById(R.id.repoId);
        int repoId = repo.getId();
        Log.v(TAG, "getView, repoID: " + repoId);
        tvRepoId.setText(Integer.toString(repoId));

        TextView tvRepoFullName = (TextView) view.findViewById(R.id.repoFullName);
        String repoName = repo.getName();
        Log.v(TAG, "getView, repoName: " + repoName);
        tvRepoFullName.setText(repoName);

        return view;
    }
}
