package at.technikum.mse.se13m035.myfragments3.githubclient;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

public interface GitHubService {
    public static final String API_URL = "https://api.github.com";

    @GET("/users/{user}/repos")
    List<Repo> listRepos(@Path("user") String user);
    @GET("/users/{user}/repos")
    void getRepos(@Path("user") String user, Callback<List<Repo>> cb);


    @GET("/repos/{owner}/{repo}/contributors")
    List<Contributor> listContributors(
            @Path("owner") String owner,
            @Path("repo") String repo
    );
    @GET("/repos/{owner}/{repo}/contributors")
    void getContributors(
            @Path("owner") String owner,
            @Path("repo") String repo,
            Callback<List<Contributor>> cb
    );
}