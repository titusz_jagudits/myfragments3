package at.technikum.mse.se13m035.myfragments3;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class StartSearchDialogFragment extends DialogFragment
{
    //    Required no-args constructor
    public StartSearchDialogFragment() {}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_start_search)
                .setPositiveButton(R.string.start_search, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((MainActivity)(StartSearchDialogFragment.this.getActivity())).onDialogButtonPressed(id);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        ((MainActivity)(StartSearchDialogFragment.this.getActivity())).onDialogButtonPressed(id);
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
