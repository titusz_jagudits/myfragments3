package at.technikum.mse.se13m035.myfragments3;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import at.technikum.mse.se13m035.myfragments3.githubclient.Contributor;

public class ContributorListFragment extends ListFragment
{
    public static final String CONTRIBUTOR_LIST = "CONTRIBUTOR_LIST";

    private static final String TAG = ContributorListFragment.class.getName();

    // holds the contributors received from bundle (fetched via REST call)
    private List<Contributor> contributors = new ArrayList();
    // reference to the activity implementing the I/F Callbacks
    private Callbacks activity = null;

    //    Required no-args constructor
    public ContributorListFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(CONTRIBUTOR_LIST)) {
            contributors = (List<Contributor>) bundle.getSerializable(CONTRIBUTOR_LIST);
        }

        ContributorListAdapter adapter = new ContributorListAdapter(getActivity(), R.layout.contributor_list_item, contributors);
        setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contributor_list, container, false);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        Log.d(TAG, "position: " + position);
        // get the contributor that was clicked on
        Contributor contributor = contributors.get(position);
        Log.d(TAG, "contributor: " + contributor);
        // call the activity to handle selection
        activity.onContributorSelected(contributor);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // save the activity that handles/implements Callbacks.onRepoSelected
        this.activity = (Callbacks) activity;
    }

    // I/F to be implemented by an activity
    public interface Callbacks {
        public void onContributorSelected(Contributor contributor);
    }
}
