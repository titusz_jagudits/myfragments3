package at.technikum.mse.se13m035.myfragments3;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import at.technikum.mse.se13m035.myfragments3.githubclient.Repo;

public class RepoListFragment extends ListFragment
{
    public static final String REPO_LIST = "REPO_LIST";

    // holds the repos received from bundle (fetched via REST call)
    private List<Repo> repos = new ArrayList();
    // reference to the activity implementing the I/F Callbacks
    private Callbacks activity = null;

    //    Required no-args constructor
    public RepoListFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(REPO_LIST)) {
            repos = (List<Repo>) bundle.getSerializable(REPO_LIST);
        }

        RepoListAdapter adapter = new RepoListAdapter(getActivity(), R.layout.repo_list_item, repos);
        setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_repo_list, container, false);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // get the repo that was clicked on
        Repo repo = repos.get(position);
        // call the activity to handle selection
        activity.onRepoSelected(repo);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // save the activity that handles/implements Callbacks.onRepoSelected
        this.activity = (Callbacks) activity;
    }

    // I/F to be implemented by an activity
    public interface Callbacks {
        public void onRepoSelected(Repo repo);
    }
}
