package at.technikum.mse.se13m035.myfragments3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import java.util.List;

import at.technikum.mse.se13m035.myfragments3.githubclient.Contributor;
import at.technikum.mse.se13m035.myfragments3.githubclient.GitHubService;
import at.technikum.mse.se13m035.myfragments3.githubclient.Repo;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RepoDetailActivity extends Activity
        implements ContributorListFragment.Callbacks
{
    private static final String TAG = RepoDetailActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_detail);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
//          Create the fragment, set its args, add it to the detail container
            RepoDetailFragment fragment = new RepoDetailFragment();
            Bundle bundle = getIntent().getBundleExtra(RepoDetailFragment.GITHUB_REPO);
            fragment.setArguments(bundle);
            getFragmentManager().beginTransaction()
                    .add(R.id.containerRepoDetail, fragment)
                    .addToBackStack(null)
                    .commit();

            Repo repo = null;
            if (bundle != null && bundle.containsKey(RepoDetailFragment.GITHUB_REPO)) {
                repo = (Repo) bundle.getSerializable(RepoDetailFragment.GITHUB_REPO);
            }

            if (repo != null) {
                // get the contributor list of the repo
                // set up the REST API adapter
                RestAdapter restAdapter = new RestAdapter.Builder()
                        .setEndpoint(GitHubService.API_URL)
                        .build();
                // create an instance of our GitHub API interface.
                GitHubService gitHubService = restAdapter.create(GitHubService.class);
                // fetch the list of contributors
                gitHubService.getContributors(
                        repo.getFull_name().split("/")[0], // owner
                        repo.getName(),
                        new Callback<List<Contributor>>() {
                            @Override
                            public void success(List<Contributor> contributors, Response response) {
                                Log.d(TAG, "getContributors callback success, contributorList" + contributors);
                                for (Contributor contributor : contributors) {
                                    Log.d(TAG, "contributor.login:" + contributor.getLogin() +
                                            ", contributions:" + contributor.getContributions());
                                }

                                // put the contributor list into a bundle
                                Bundle bundle = new Bundle();
                                bundle.putSerializable(ContributorListFragment.CONTRIBUTOR_LIST,
                                        (java.io.Serializable) contributors);

                                // create the list fragment, hand the bundle over and show it
                                ContributorListFragment clf = new ContributorListFragment();
                                clf.setArguments(bundle);
                                getFragmentManager().beginTransaction()
                                        //.add(R.id.containerList, repoListFragment)
                                        //.add(R.id.containerMain, repoListFragment)
                                        //.add(R.id.fragmentRepoList, repoListFragment)
                                        .replace(R.id.fragmentContributorList, clf)
                                        .addToBackStack(null)
                                        .commit();
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Log.d(TAG, "getContributors callback failure, error" + error);
                                // TODO handle REST API call failure
                            }
                        });
            }
        }
    }

    //  Returns to the list activity
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    @Override
    public void onContributorSelected(Contributor contributor) {
        Log.d(TAG, "onRepoSelected called, contributor: " + contributor);

        Bundle b = new Bundle();
        b.putString(MainActivity.USER_TO_SEARCH_FOR, contributor.getLogin());
        Log.d(TAG, "contributor.login: " + contributor.getLogin());
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(MainActivity.USER_TO_SEARCH_FOR, b);
        startActivity(intent);
        finish();
    }
}
