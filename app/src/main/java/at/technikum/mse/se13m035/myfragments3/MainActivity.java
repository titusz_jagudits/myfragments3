package at.technikum.mse.se13m035.myfragments3;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import at.technikum.mse.se13m035.myfragments3.githubclient.Contributor;
import at.technikum.mse.se13m035.myfragments3.githubclient.GitHubService;
import at.technikum.mse.se13m035.myfragments3.githubclient.Repo;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends Activity
        implements RepoListFragment.Callbacks, ContributorListFragment.Callbacks
{
    public static final String USER_TO_SEARCH_FOR = "USER_TO_SEARCH_FOR";

    private static final String TAG = MainActivity.class.getName();
    private static final String API_URL = "https://api.github.com";
    // TODO might be specified more precisely
    private static final String INVALID_SEARCH_TERM_REGEXP = "[\\W]+";

    private String userToSearch = "";
    private RepoListFragment repoListFragment = null;
    private RepoDetailFragment repoDetailFragment = null;
    private ContributorListFragment contributorListFragment = null;
    private ProgressDialog progressDialog;
    private GitHubService gitHubService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize the GitHub REST API service
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(GitHubService.API_URL)
                .build();
        gitHubService = restAdapter.create(GitHubService.class);
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        // check network availability
        if (!isOnline()) {
            // show error message in dialog
            NoNetworkDialogFragment nndf = new NoNetworkDialogFragment();
            nndf.show(getFragmentManager(), "NoNetworkDialogFragment TAG");
            // pushing the button of nndf finishes the application
        } else {
            // fetch user from bundle that was choosen in detail view/contributor list
            Bundle bundle = getIntent().getBundleExtra(USER_TO_SEARCH_FOR);
            if (bundle != null && bundle.containsKey(USER_TO_SEARCH_FOR)) {
                userToSearch = bundle.getString(USER_TO_SEARCH_FOR);
                // set the content of edit text
                EditText editText = (EditText) findViewById(R.id.edit_message);
                editText.setText(userToSearch);
                sendMessage(findViewById(R.id.send_button));
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        // called when back button is pressed
        // pop back last transaction from back stack
        if (getFragmentManager().getBackStackEntryCount() > 0){
            getFragmentManager().popBackStack();
        } else {
            // call default action on back button pressed
            super.onBackPressed();
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /** Called when the user clicks the Send button */
    public void sendMessage(View view) {
        Log.d(TAG, "sendMessage called, view: " + view);

        // get content of edit text
        EditText editText = (EditText) findViewById(R.id.edit_message);
        userToSearch = editText.getText().toString();
        Log.d(TAG, "userToSearch.length:" + userToSearch.length() + ", userToSearch:" + userToSearch);

        // check: userToSearch should contain some "meaningful" characters
        if (userToSearch == null || userToSearch.isEmpty() || userToSearch.matches(INVALID_SEARCH_TERM_REGEXP)) {
            // show error dialog
            InvalidSearchTermDialogFragment istdf = new InvalidSearchTermDialogFragment();
            istdf.show(getFragmentManager(), "InvalidSearchTermDialogFragment TAG");
        } else {
//            // just for fun: let the user confirm the search via dialog
//            StartSearchDialogFragment ssd = new StartSearchDialogFragment();
//            ssd.show(getFragmentManager(), "StartSearchDialogFragment TAG");

            onDialogButtonPressed(DialogInterface.BUTTON_POSITIVE);
        }
    }

    /** Called when user taps a button on the StartSearchDialogFragment */
    public void onDialogButtonPressed(int which) {
        Log.d(TAG, "onDialogButtonPressed called");

        if (which == DialogInterface.BUTTON_POSITIVE)
        {
            // start progress dialog while searching
            progressDialog = ProgressDialog.show(this, "Searching for", "repos of " + userToSearch + " ...", true, true);

            // fetch the list of repositories of the given github user (userToSearch)
            gitHubService.getRepos(userToSearch, new Callback<List<Repo>>() {
                @Override
                public void success(List<Repo> repoList, Response response) {
                    Log.d(TAG, "getRepos callback success, repoList" + repoList);
                    for (Repo repo : repoList) {
                        Log.v(TAG, "repo.ID:" + repo.getId() + ", full name:" + repo.getName());
                    }

                    progressDialog.dismiss();

                    if (repoList.isEmpty()) {
                        Bundle b = new Bundle();
                        b.putString(ErrorDialogFragment.TITLE, "GitHub user has NO repo");
                        b.putString(ErrorDialogFragment.MESSAGE, response.toString());
                        b.putSerializable(ErrorDialogFragment.BUTTON, "OK");
                        ErrorDialogFragment edf = new ErrorDialogFragment();
                        edf.setArguments(b);
                        edf.show(getFragmentManager(), "ErrorDialogFragment TAG");
                    } else {
                        // put the repo list into a bundle
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(RepoListFragment.REPO_LIST, (java.io.Serializable) repoList);

                        // create the list fragment, hand the bundle over and show it
                        // TODO some refactoring might be done here
                        RepoListFragment rlf = new RepoListFragment();
                        rlf.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();

                        if (repoDetailFragment != null)
                            transaction.remove(repoDetailFragment);
                        repoDetailFragment = null;
                        contributorListFragment = null;

                        if (repoListFragment == null) {
                            transaction.add(R.id.containerListAndDetails    // R.id.containerRepoList
                                    , rlf);                                 // R.id.containerMain
                        } else {
                            transaction.replace(R.id.containerListAndDetails    //R.id.containerRepoList
                                    , rlf);                                     // R.id.fragmentRepoList
                        }
                        repoListFragment = rlf;
                        transaction.addToBackStack(null).commit();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d(TAG, "getRepos callback failure, error" + error);

                    progressDialog.dismiss();

                    // Open error dialog
                    Bundle b = new Bundle();
                    b.putString(ErrorDialogFragment.TITLE, "Unknown GitHub user");
                    b.putString(ErrorDialogFragment.MESSAGE, error.getMessage());
                    b.putSerializable(ErrorDialogFragment.BUTTON, "Understand");
                    ErrorDialogFragment edf = new ErrorDialogFragment();
                    edf.setArguments(b);
                    edf.show(getFragmentManager(), "ErrorDialogFragment TAG");
                }
            });
        }

        if (which == DialogInterface.BUTTON_NEGATIVE) {
            showToast("Search is cancelled", Toast.LENGTH_SHORT);
            // Search is cancelled => nothing to do
        }
    }

    @Override
    public void onRepoSelected(Repo repo)
    {
        Log.d(TAG, "onRepoSelected, repo: " + repo.getName());
        Bundle b = new Bundle();
        b.putSerializable(RepoDetailFragment.GITHUB_REPO, repo);

        RepoDetailFragment rdf = new RepoDetailFragment();
        rdf.setArguments(b);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Log.d(TAG, "onRepoSelected, repoListFragment: " + repoListFragment);
        if (repoListFragment != null)
            transaction.remove(repoListFragment);
        repoListFragment = null;

        Log.d(TAG, "onRepoSelected, repoDetailFragment: " + repoDetailFragment);
        if (repoDetailFragment == null) {
            transaction.add(R.id.containerListAndDetails //R.id.containerRepoDetail
                    , rdf);
        } else {
            transaction.replace(R.id.containerListAndDetails //R.id.containerRepoDetail
                    , rdf);
        }
        repoDetailFragment = rdf;
        transaction.addToBackStack(null).commit();

        fetchAndShowContributors(repo);
    }

    @Override
    public void onContributorSelected(Contributor contributor)
    {
        userToSearch = contributor.getLogin();
        Log.d(TAG, "onContributorSelected, contributor.login: " + userToSearch);

//        getFragmentManager().beginTransaction()
//                .remove(repoDetailFragment)
//                .commit();
//        repoDetailFragment = null;

        // set the content of edit text
        EditText editText = (EditText) findViewById(R.id.edit_message);
        editText.setText(userToSearch);
//        sendMessage(findViewById(R.id.send_button));

        onDialogButtonPressed(DialogInterface.BUTTON_POSITIVE);
    }

    private void showToast(String message, int length) {
        Toast toast = Toast.makeText(getApplicationContext(), message, length);
        toast.show();
    }

    private void fetchAndShowContributors(Repo repo)
    {
        Log.d(TAG, "fetchAndShowContributors, repo: " + repo);
        if (repo != null) {
            // get the contributor list of the repo
            // set up the REST API adapter
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(GitHubService.API_URL)
                    .build();
            // create an instance of our GitHub API interface.
            GitHubService gitHubService = restAdapter.create(GitHubService.class);
            // fetch the list of contributors
            gitHubService.getContributors(
                    repo.getFull_name().split("/")[0], // owner
                    repo.getName(),
                    new Callback<List<Contributor>>() {
                        @Override
                        public void success(List<Contributor> contributors, Response response) {
                            Log.d(TAG, "getContributors callback success, contributorList" + contributors);
                            for (Contributor contributor : contributors) {
                                Log.v(TAG, "contributor.login:" + contributor.getLogin() +
                                        ", contributions:" + contributor.getContributions());
                            }

                            // put the contributor list into a bundle
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(ContributorListFragment.CONTRIBUTOR_LIST,
                                    (java.io.Serializable) contributors);

                            // create the list fragment, hand the bundle over and show it
                            ContributorListFragment clf = new ContributorListFragment();
                            clf.setArguments(bundle);

                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            if (contributorListFragment == null) {
                                transaction.add(R.id.containerContributorList
                                        , clf);
                            } else {
                                transaction.replace(R.id.containerContributorList
                                        , clf);
                            }
                            contributorListFragment = clf;
                            transaction.addToBackStack(null).commit();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.d(TAG, "getContributors callback failure, error" + error);
                            // TODO handle REST API call failure
                        }
                    });
        }
    }
}
