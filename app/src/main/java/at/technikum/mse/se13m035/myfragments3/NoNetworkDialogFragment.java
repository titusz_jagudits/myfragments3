package at.technikum.mse.se13m035.myfragments3;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

public class NoNetworkDialogFragment extends DialogFragment
{
    private static final String TAG = NoNetworkDialogFragment.class.getName();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.no_nw_dialog_title)
                .setMessage(R.string.no_nw_dialog_message)
                .setNeutralButton(R.string.no_nw_dialog_button,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User clicked OK button
                                Log.d(TAG, "onClick, dialog: " + dialog);
                                Log.d(TAG, "onClick, activity: " + getActivity());
                                getActivity().finish();
                            }
                        }
                );

        return builder.create();
    }


}
