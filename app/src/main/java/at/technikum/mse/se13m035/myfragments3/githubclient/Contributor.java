package at.technikum.mse.se13m035.myfragments3.githubclient;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Contributor {
    String login;
    int contributions;
}
