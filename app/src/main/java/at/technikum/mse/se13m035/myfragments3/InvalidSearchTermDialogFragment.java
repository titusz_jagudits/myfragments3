package at.technikum.mse.se13m035.myfragments3;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class InvalidSearchTermDialogFragment extends DialogFragment
{
    //    Required no-args constructor
    public InvalidSearchTermDialogFragment() {}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.invalid_search_term_dialog_title)
                .setMessage(R.string.invalid_search_term_dialog_message)
                .setNeutralButton(R.string.invalid_search_term_dialog_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button => nothing to do
                    }
                });
        return builder.create();
    }
}
