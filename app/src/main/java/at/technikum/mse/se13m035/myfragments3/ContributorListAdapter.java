package at.technikum.mse.se13m035.myfragments3;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import at.technikum.mse.se13m035.myfragments3.githubclient.Contributor;
import at.technikum.mse.se13m035.myfragments3.githubclient.Repo;

public class ContributorListAdapter extends ArrayAdapter<Contributor>
{
    private static final String TAG = ContributorListAdapter.class.getName();

    private Context context;
    private List<Contributor> objects;

    public ContributorListAdapter(Context context, int resource, List<Contributor> objects) {
        super(context, resource, objects);
        this.context = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Contributor contributor = objects.get(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//        View view = inflater.inflate(R.layout.contributor_list_item, null);
        View view = inflater.inflate(R.layout.contributor_list_item, parent, false);

        TextView tvContributorLogin = (TextView) view.findViewById(R.id.contributorLogin);
        String contrLogin = contributor.getLogin();
        Log.v(TAG, "getView, contributorLogin: " + contrLogin);
        tvContributorLogin.setText(contrLogin);

        TextView tvContributorContributions = (TextView) view.findViewById(R.id.contributorContributions);
        int contrContributions = contributor.getContributions();
        Log.v(TAG, "getView, contributions: " + contrContributions);
        tvContributorContributions.setText(Integer.toString(contrContributions));

        return view;
    }
}
