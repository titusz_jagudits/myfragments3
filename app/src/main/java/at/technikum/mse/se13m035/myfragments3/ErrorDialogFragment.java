package at.technikum.mse.se13m035.myfragments3;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

public class ErrorDialogFragment extends DialogFragment
{
    public static final String TAG = ErrorDialogFragment.class.getName();
    public static final String TITLE = "TITLE";
    public static final String MESSAGE = "MESSAGE";
    public static final String BUTTON = "BUTTON";


    //    Required no-args constructor
    public ErrorDialogFragment() {}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        String title = getResources().getString(R.string.default_error_dialog_title);
        String message = getResources().getString(R.string.default_error_dialog_message);
        String button = getResources().getString(R.string.default_error_dialog_button);
        Bundle bundle = getArguments();
        Log.v(TAG, "onCreateDialog, bundle: " + bundle);
        if (bundle != null) {
            title = bundle.getString(TITLE, getResources().getString(R.string.default_error_dialog_title));
            message = bundle.getString(MESSAGE, getResources().getString(R.string.default_error_dialog_message));
            button = bundle.getString(BUTTON, getResources().getString(R.string.default_error_dialog_button));
        }

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setMessage(message)
                .setNeutralButton(button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User OKed the dialog
                        // nothing to do
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
