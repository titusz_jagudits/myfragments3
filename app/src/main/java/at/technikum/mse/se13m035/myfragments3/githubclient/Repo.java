package at.technikum.mse.se13m035.myfragments3.githubclient;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Repo implements Serializable {
    private int id;
    private String name;
    private String full_name;
    private String description;
    private Date updated_at;
}
