/*
* Copyright (C) 2012 Square, Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package at.technikum.mse.se13m035.myfragments3.githubclient;

import android.os.AsyncTask;
import android.util.Log;

import java.util.List;
import retrofit.RestAdapter;

public class GitHubAsyncClient extends AsyncTask<String, Void, Void> {

    private static final String API_URL = "https://api.github.com";
    private static final String TAG = GitHubAsyncClient.class.getName();

    private RestAdapter restAdapter;
    private GitHubService gitHubService;

    public GitHubAsyncClient() {
        // Create a very simple REST adapter which points the GitHub API endpoint.
        restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .build();

        // Create an instance of our GitHub API interface.
        gitHubService = restAdapter.create(GitHubService.class);
    }

    @Override
    protected Void doInBackground(String... params) {
        if (params.length != 3) {
            Log.e(TAG, "doInBackground expects 3 params (params.length is " + params.length + ")");
            return null;
        }

        Log.d(TAG, "doInBackground started ...");
        logContributors(params[0], params[1]);
        logReposOfUser(params[2]);

        return null;
    }

    public void logContributors(String owner, String repo) {
        // Fetch and print a list of the contributors to this library.
        List<Contributor> contributors = gitHubService.listContributors(owner, repo);
        for (Contributor contributor : contributors) {
            Log.d(TAG, contributor.login + " (" + contributor.contributions + ")");
        }
    }

    private void logReposOfUser(String userName) {
        List<Repo> repos = gitHubService.listRepos(userName);
        for (Repo repo : repos) {
            Log.d(TAG, "repo ID:" + repo.getId() + ", full name:" + repo.getName());
        }
    }
}